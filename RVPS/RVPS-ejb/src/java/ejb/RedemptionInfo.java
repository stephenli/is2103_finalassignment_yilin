/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author liyilin
 */
@Entity
public class RedemptionInfo implements Serializable {

    @Id
    private String RedemptionInfoId;
    private String customerName;
    private String customerAddress;
    private Long rewardId;
    private String rewardName;
    private int rewardQuantity;
    private String merchantName;
    private String merchantAddress;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date redemptionDate;

    public RedemptionInfo() {
    }

    public RedemptionInfo(String RedemptionInfoId, String customerName,
            String customerAddress, Long rewardId, String rewardName,
            int rewardQuantity, String merchantName, String merchantAddress) {
        this.RedemptionInfoId = RedemptionInfoId;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.rewardId = rewardId;
        this.rewardName = rewardName;
        this.rewardQuantity = rewardQuantity;
        this.merchantName = merchantName;
        this.merchantAddress = merchantAddress;
        this.redemptionDate = redemptionDate;
        Calendar c = Calendar.getInstance();
        this.redemptionDate = c.getTime();
    }

    public String getRedemptionInfoId() {
        return RedemptionInfoId;
    }

    public void setRedemptionInfoId(String RedemptionInfoId) {
        this.RedemptionInfoId = RedemptionInfoId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMerchantAddress() {
        return merchantAddress;
    }

    public void setMerchantAddress(String merchantAddress) {
        this.merchantAddress = merchantAddress;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public Date getRedemptionDate() {
        return redemptionDate;
    }

    public void setRedemptionDate(Date redemptionDate) {
        this.redemptionDate = redemptionDate;
    }

    public Long getRewardId() {
        return rewardId;
    }

    public void setRewardId(Long rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public int getRewardQuantity() {
        return rewardQuantity;
    }

    public void setRewardQuantity(int rewardQuantity) {
        this.rewardQuantity = rewardQuantity;
    }


}
