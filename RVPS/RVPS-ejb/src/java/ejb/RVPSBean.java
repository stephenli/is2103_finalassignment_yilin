/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author liyilin
 */
@MessageDriven(mappedName = "jms/Queue", activationConfig = {
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class RVPSBean implements MessageListener {

    @PersistenceContext
    EntityManager em;

    public RVPSBean() {
    }

    public void onMessage(Message message) {
        MapMessage msg = null;
        try {
            msg = (MapMessage) message;
            setEntity(msg);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setEntity(MapMessage msg) {
        try {
            String redemptionInfoId = msg.getString("RedemptionInfoId");
            String customerName = msg.getString("CustomerName");
            String customerAddress = msg.getString("CustomerAddress");
            Long rewardID = msg.getLong("RewardID");
            String rewardName = msg.getString("RewardName");
            int quantity = msg.getInt("quantity");
            String merchantName = msg.getString("MerchantName");
            String merchantAddress = msg.getString("MerchantAddress");
            RedemptionInfo ri = new RedemptionInfo(redemptionInfoId, customerName,
                    customerAddress, rewardID, rewardName,
                    quantity, merchantName, merchantAddress);
            em.persist(ri);
            em.flush();
            System.out.println("RVPSBean.setEntity: done");
        } catch (JMSException ex) {
            Logger.getLogger(RVPSBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
