/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author liyilin
 */
@Remote
public interface RedemptionInfoManagerRemote {

    public List<RedemptionInfo> getRedemptionInfo();

    public RedemptionInfo getRedemptionInfo(String id);
}
