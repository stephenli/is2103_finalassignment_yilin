package ejb;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateful
public class RedemptionInfoManagerBean implements RedemptionInfoManagerRemote {

    @PersistenceContext
    EntityManager em;

    public List<RedemptionInfo> getRedemptionInfo() {
        List<RedemptionInfo> stateList = new ArrayList<RedemptionInfo>();
        try {
            Query q = em.createQuery("SELECT c FROM RedemptionInfo c");
            for (Object o : q.getResultList()) {
                RedemptionInfo r = (RedemptionInfo) o;
                stateList.add(r);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println(stateList.toString());
        return stateList;
    }

    public RedemptionInfo getRedemptionInfo(String id) {
        RedemptionInfo red = null;
        for (Object o : getRedemptionInfo()) {
            RedemptionInfo temp = (RedemptionInfo) o;
            if (temp.getRedemptionInfoId().equals(id)) {
                red = temp;
            }
        }
        return red;
    }
}
