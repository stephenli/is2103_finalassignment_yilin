/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rvps;

import ejb.RedemptionInfo;
import ejb.RedemptionInfoManagerRemote;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import javax.naming.InitialContext;

/**
 *
 * @author liyilin
 */
public class Selection {

    private RedemptionInfoManagerRemote rim;

    public Selection() {
    }

    public void rvps(String[] args) {
        getSessionBean();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        try {
            String choice = "";
            while (!choice.equals("0")) {
                displayMenu();
                choice = br.readLine();
                dispatch(choice);
            }
            return;
        } catch (Exception ex) {
            System.err.println("\nERROR: Caught an " +
                    "unexpected exception");
        }
    }

    public void displayMenu() {
        System.out.println("\n\n\t\tWELCOME TO RVPS\n");
        System.out.println("Welcome to Redemption Voucher Printing System");
        System.out.println("1. List avaliable redemption information:");
        System.out.println("2. Print redemption voucher");
        System.out.println("0. Exit");
    }

    public void dispatch(String choice) {
        if (choice.equals("1")) {
            ListRedemption();
        } else if (choice.equals("2")) {
            PrintVoucher();
        } else if (choice.equals("0")) {
            return;
        } else {
            System.out.println("\nERROR: Invalid choice.");
        }
    }

    private void PrintVoucher() {
        System.out.println("Please enter redemption id:");

        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));

        String id = null;
        try {
            id = br.readLine();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        RedemptionInfo red = rim.getRedemptionInfo(id);
        System.out.println();
        System.out.println("======================================");
        System.out.println("Redemtion ID: " + red.getRedemptionInfoId());
        System.out.println("======================================");
        System.out.println("Customer Name: " + formatString(red.getCustomerName(), 10));
        System.out.println("Customer Address: " + formatString(red.getCustomerAddress(), 15));
        System.out.println("--------");
        System.out.println("Reward ID: " + formatString(red.getRewardId().toString(), 14));
        System.out.println("Reward Name: " + formatString(red.getRewardName(), 15));
        System.out.println("Reward Quantity: " + red.getRewardQuantity());
        System.out.println("--------");
        System.out.println("Merchant Name: " + formatString(red.getMerchantName(), 10));
        System.out.println("Merchant Address: " + red.getMerchantAddress());
        System.out.println("--------");
        System.out.printf("%1$s %2$tB %2$td, %2$tY", "Date:", red.getRedemptionDate());
        System.out.println();
        System.out.println("======================================");

    }

    private void ListRedemption() {
        try {
            List<RedemptionInfo> ris = rim.getRedemptionInfo();
            if (ris != null) {
                for (Object o : ris) {
                    RedemptionInfo ri = (RedemptionInfo) o;
                    System.out.println("====================================================");
                    System.out.println("Redemtion ID: " + ri.getRedemptionInfoId());
                    System.out.println("====================================================");
                    System.out.println("Customer Name: " + formatString(ri.getCustomerName(), 10) +
                            "Customer Address: " + formatString(ri.getCustomerAddress(), 15));
                    System.out.println("Reward ID: " + formatString(ri.getRewardId().toString(), 14) +
                            "Reward Name: " + formatString(ri.getRewardName(), 15) + "Reward Quantity: " +
                            "" +
                            ri.getRewardQuantity());
                    System.out.println("Merchant Name: " + formatString(ri.getMerchantName(), 10) +
                            "Merchant Address: " + ri.getMerchantAddress());
                    System.out.printf("%1$s %2$tB %2$td, %2$tY", "Date:", ri.getRedemptionDate());
                    System.out.println();
                }
            } else {
                System.out.println("Redemption Info is empty!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String formatString(String s, int len) {
        String out = s;

        for (int i = 0; i < (len - s.length()); i++) {
            out = out + " ";
        }
        return out;
    }

    public void getSessionBean() {
        try {
            InitialContext ic = new InitialContext();
            rim = (RedemptionInfoManagerRemote) ic.lookup(RedemptionInfoManagerRemote.class.getName());
        } catch (Exception ex) {
            System.out.println("Initial Context lookup exception.");
            ex.printStackTrace();
        }
    }
}
