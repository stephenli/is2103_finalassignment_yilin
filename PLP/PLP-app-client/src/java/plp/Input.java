package plp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Input {

    public Input() {
    }

    public String getString(String attrName, String oldValue) {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        String stringValue = null;

        try {
            while (true) {
                System.out.print("Enter " + attrName +
                        (oldValue == null ? "" : "(" + oldValue +
                        ")") + " : ");
                stringValue = br.readLine();
                if (stringValue.length() != 0) {
                    break;
                } else if (stringValue.length() == 0 &&
                        oldValue != null) {
                    stringValue = oldValue;
                    break;
                }
                System.out.println("Invalid " + attrName + "...");
            }//end while
        } catch (Exception ex) {
            System.out.println("\nSystem Error Message: " +
                    ex.getMessage() + "\n");
        }
        return stringValue.trim();
    }

    public int getInt(String attrName, int oldValue) {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        int newValue = 0;

        try {
            while (true) {
                System.out.print("Enter " + attrName +
                        (oldValue == 0 ? "" : "(" + oldValue +
                        ")") + " : ");
                String s = br.readLine();

                try {
                    newValue = Integer.parseInt(s);
                } catch (Exception ex) {
                    System.out.println("\nERROR: Can not parse to integer");
                }
                if (newValue != 0) {
                    break;
                } else if (newValue == 0 &&
                        oldValue != 0) {
                    newValue = oldValue;
                    break;
                }
                System.out.println("Invalid " + attrName + "...");
            }//end while
        } catch (Exception ex) {
            System.out.println("\nSystem Error Message: " +
                    ex.getMessage() + "\n");
        }
        return newValue;
    }

    public HashSet getMerchants() {
        String merId;
        HashSet merIds = new HashSet();
        System.out.println("\n\n\t==Enter Merchant ID==\n");
        System.out.println("\t\t--------------------");
        boolean more = true;
        while (more) {
            try {
                System.out.println();
                merId = getString("Merchant Id", null);
                merIds.add(merId);
                if (!getYesNo("\nDo you want to add more merchants?")) {
                    more = false;
                }
            } catch (Exception ex) {
                System.out.println("\nERROR: (get merchant)" + ex.getMessage());
            }
        }
        return merIds;
    }
    /*
    public ArrayList getRedemption() {

    int rwdQn;

    System.out.println("\n\n\t==Enter Reward Type ID==\n");
    System.out.println("\t\t--------------------");
    boolean more = true;
    ArrayList red = new ArrayList();
    while (more) {
    try {
    System.out.println();
    int rwdId = getInt("Reward Type Id", 0);
    //test
    System.out.println(rwdId);
    rwdQn = getInt("Quantity", 0);

    //test
    System.out.println(rwdQn);
    red.add(rwdId);
    red.add(rwdQn);
    if (!getYesNo("\nDo you want to add more Reward Type?")) {
    more = false;
    }
    } catch (Exception ex) {
    System.out.println("\nERROR: " + ex.getMessage());
    }
    }
    return red;
    }
     */

    public List<Long> deleteRedemption() {


        boolean more = true;
        ArrayList redId = new ArrayList();
        while (more) {
            try {
                Integer temp = getInt("Id", 0);
                Long id = temp.longValue();
                redId.add(id);
                if (!getYesNo("\nDo you want to delete more?")) {
                    more = false;
                }
            } catch (Exception ex) {
                System.out.println("\nERROR: " + ex.getMessage());
            }
        }
        return redId;
    }

    public List<Long> makeClaim() {


        boolean more = true;
        ArrayList redId = new ArrayList();
        while (more) {
            try {
                Integer temp = getInt("Id", 0);
                Long id = temp.longValue();
                redId.add(id);
                if (!getYesNo("\nDo you want to claim more?")) {
                    more = false;
                }
            } catch (Exception ex) {
                System.out.println("\nERROR: " + ex.getMessage());
            }
        }
        return redId;
    }

    public boolean getYesNo(String message) {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        String yn = null;
        try {
            while (true) {
                System.out.println(message + " (Y/N) ");

                yn = br.readLine();
                if (yn != "") {
                    if (yn.toUpperCase().equals("Y")) {
                        return true;
                    } else if (yn.toUpperCase().equals("N")) {
                        return false;
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("\nERROR: (get yes/no) " + ex.getMessage());
        }
        return false;
    }
}
