package plp;

import Manager.CustomerContactManagerRemote;
import Manager.MerchantManagerRemote;
import Manager.RedemptionManagerRemote;
import Manager.RewardTypeMerchantManagerRemote;
import javax.ejb.EJB;

public class Main {

    @EJB
    public static CustomerContactManagerRemote ccm;
    @EJB
    public static MerchantManagerRemote mm;
    @EJB
    public static RewardTypeMerchantManagerRemote rmm;
    @EJB
    public static RedemptionManagerRemote rm;

    public Main() {
    }

    public static void main(String[] args) {

        new Selection().plp(args);
    }

    
    
}


