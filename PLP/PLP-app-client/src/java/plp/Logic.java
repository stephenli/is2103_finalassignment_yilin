package plp;

import Manager.*;
import exception.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.naming.InitialContext;

public class Logic {

    private CustomerContactManagerRemote ccm;
    private MerchantManagerRemote mm;
    private RewardTypeMerchantManagerRemote rmm;
    private RedemptionManagerRemote rm;
    Input i = new Input();

    public Logic() {

        getCCMSessionBean();
        getMMSessionBean();
        getRMMSessionBean();
        getRMSessionBean();
    }

    public void getRMSessionBean() {
        try {
            InitialContext ic = new InitialContext();
            rm = (RedemptionManagerRemote) ic.lookup(RedemptionManagerRemote.class.getName());
        } catch (Exception ex) {
            System.out.println("Initial Context lookup exception.");
            ex.printStackTrace();
        }
    }

    public void getCCMSessionBean() {
        try {
            InitialContext ic = new InitialContext();
            ccm = (CustomerContactManagerRemote) ic.lookup(CustomerContactManagerRemote.class.getName());
        } catch (Exception ex) {
            System.out.println("Initial Context lookup exception.");
            ex.printStackTrace();
        }
    }

    public void getRMMSessionBean() {
        try {
            InitialContext ic = new InitialContext();
            rmm = (RewardTypeMerchantManagerRemote) ic.lookup(RewardTypeMerchantManagerRemote.class.getName());
        } catch (Exception ex) {
            System.out.println("Initial Context lookup exception.");
            ex.printStackTrace();
        }
    }

    public void getMMSessionBean() {
        try {
            InitialContext ic = new InitialContext();
            mm = (MerchantManagerRemote) ic.lookup(MerchantManagerRemote.class.getName());
        } catch (Exception ex) {
            System.out.println("Initial Context lookup exception.");
            ex.printStackTrace();
        }
    }

    public void addCustomer() {
        String custId;
        String custName;
        String custPass;
        String custAddress;
        try {
            System.out.println("\n\n\t\t==Add a customer==\n");
            custId = i.getString("Customer Id", null);
            custName = i.getString("Customer Name", null);
            custPass = i.getString("Customer Password", null);
            custAddress = i.getString("Customer Address", null);

            ccm.createCustomer(custId, custName, custPass, custAddress);
            System.out.println("Customer " + custId + " successfully created.");

        } catch (ExistException ex) {
            System.out.println("\nERROR: Failed to add customer");
            System.out.println(ex.getMessage() + "\n");
        } catch (Exception ex) {
            System.out.println("\nERROR: Failed to add customer");
            System.out.println("UNKOWN ERROR\n");
        }

    }

    public void deleteCustomer() {
        String custId;
        try {
            System.out.println("\n\n\t\t==Delete a customer==\n");
            custId = i.getString("Customer Id", null);
            ccm.deleteCustomer(custId);
            System.out.println("Customer " + custId + " successfully removed.");

        } catch (ExistException ex) {
            System.out.println("\nERROR: Failed to delete customer");
            System.out.println(ex.getMessage() + "\n");
        } catch (CustomerException ex) {
            System.out.println("\nERROR: Failed to delete customer");
            if (ex.getException() == CustomerException.getHAS_REDEMPTION()) {
                System.out.println("CUSTOMER HAS REDEMPTION");
            }

        } catch (Exception ex) {
            System.out.println("\nERROR: Failed to delete customer");
            System.out.println("UNKOWN ERROR\n");
        }

    }

    public void addMerchant() {
        String merId;
        String merName;

        String merAddress;
        try {
            System.out.println("\n\n\t\t==Add a Merchant==\n");
            merId = i.getString("Merchant Id", null);
            merName = i.getString("Merchant Name", null);
            merAddress = i.getString("Merchant Address", null);

            mm.createMerchant(merId, merName, merAddress);
            System.out.println("Merchant " + merId + " successfully created.");

        } catch (ExistException ex) {
            System.out.println("\nERROR: Failed to add customer");
            System.out.println(ex.getMessage() + "\n");
        } catch (Exception ex) {
            System.out.println("\nERROR: Failed to add customer");
            System.out.println("UNKOWN ERROR\n");
        }
    }

    public void deleteMerchant() {
        String merId;
        try {
            System.out.println("\n\n\t\t==Delete a merchant==\n");
            merId = i.getString("Merchant Id", null);
            mm.deleteMerchant(merId);
            System.out.println("Merchant " + merId + " successfully removed.");

        } catch (ExistException ex) {
            System.out.println("\nERROR: Failed to delete merchant");
            System.out.println(ex.getMessage() + "\n");
        } catch (MerchantException ex) {
            System.out.println("\nERROR: Failed to delete merchant");
            if (ex.getException() == ex.getHAS_REWARD()) {
                System.out.println("MERCHANT HAS REWARD");
            }
        } catch (Exception ex) {
            System.out.println("\nERROR: Failed to delete merchant");
            System.out.println("UNKOWN ERROR\n");
        }
    }

    public void addRewardType() {
        HashSet<String> merIds = new HashSet();
        System.out.println("\n\n\t\t==Add a reward type==\n");
        System.out.println(mm.ListMerchants());
        merIds = i.getMerchants();
        System.out.println(merIds.toString());
        if (!rmm.linkMerchants(merIds)) {
            System.out.println("Merchant Id input wrongly, please try again.");
        }

        String name = i.getString("Reward Type Name", null);
        int point = i.getInt("Point reuired(*100)", 0) * 100;
        int quantity = i.getInt("Quantity avaliable", 0);
        try {
            boolean created = rmm.createRewardType(name, point, quantity);
            if (created) {
                rmm.persist();
                System.out.println("Reward Type " + name + " successfully created.");
            } else {
                System.out.println("Reward Type " + name +
                        " can not be created. Point required already in the database. Please try again.");
            }
        } catch (Exception ex) {
            System.out.println("\nERROR: Failed to add reward type.");
            System.out.println("UNKOWN ERROR\n");
        }
    }

    public void deleteRewardType() {
        System.out.println("\n\n\t\t==Delete RewardType==" +
                "\n");
        System.out.println(rmm.ListRewardType());
        System.out.println("Please input the ID:");
        Integer id = (Integer) i.getInt("Reward Type ID", 0);
        Long LongId = id.longValue();
        try {
            String response = rmm.deleteRewardType(LongId);
            System.out.println(response);
        } catch (Exception ex) {
            System.out.println("\nERROR: Failed to add reward type.");
            System.out.println("UNKOWN ERROR\n");
        }
    }

    /*  public void addRedemption() {
    System.out.println("\n\n\t\t==Add redemption==" +
    "\n");
    boolean login = false;
    String custId = null;
    while (!login) {
    System.out.print("\n\t==Login==\t\n");
    String id = i.getString("Id", null);
    String password = i.getString("Password", null);
    login = rm.login(id, password);
    if (login) {
    custId = id;
    } else {
    System.out.println("\nWrong ID or Password, please try again\n");
    }
    }
    rm.createRedemption();
    System.out.println("\n" + rmm.ListRewardType());
    System.out.println("Hello " + rm.showCustName(custId) + "!");

    try {
    ArrayList red = i.getRedemption();
    for (int i = 0; i < red.size(); i += 2) {
    Integer id = (Integer) red.get(i);
    long Lid=id.longValue();

    int Qn = (Integer) red.get(i + 1);


    String response = rm.createRedemptionLine(Lid, Qn);
    System.out.println(response);
    }


    } catch (ExistException ex) {
    System.out.println("\nERROR: Failed to add redemption");
    System.out.println(ex.getMessage() + "\n");
    } catch (RedemptionException ex) {
    System.out.println("\nERROR: Failed to add redemption");
    if (ex.getException() == RedemptionException.getINSUFFICIENT_POINT()) {
    System.out.println("INSUFFICIENT POINT!");
    }
    } catch (Exception ex) {
    System.out.println("\nERROR: Failed to add redemption");
    System.out.println("UNKOWN ERROR\n");
    System.out.println(ex.getMessage());
    }

    }
     */
    public void deleteRedemption() {
        System.out.println("\n\n\t\t==Delete redemption==" +
                "\n");
        boolean login = false;
        String custId = null;
        while (!login) {
            System.out.print("\n\t==Login==\t\n");
            String id = i.getString("Your Id", custId);
            String password = i.getString("Password", null);

            System.out.println(id + "\n" + password);
            login = rm.login(id, password);
            if (login) {
                custId = id;
            } else {
                System.out.println("\nWrong ID or Password, please try again\n");
            }

        }
        System.out.println("\nHello " + rm.showCustName(custId) + "!\n");
        System.out.println(rm.ListRedemption());
        try {
            List<Long> ids = i.deleteRedemption();
            for (Object o : ids) {
                Long id = (Long) o;
                System.out.println(rm.deleteRedemption(id));
            }
        } catch (RedemptionException ex) {
            System.out.println("\nERROR: Failed to delete redemption");
            System.out.println(ex.getMessage() + "\n");
        }
    }

    void claim() {
        System.out.println("\n\n\t\t==Claim Redemption==" +
                "\n");
        boolean login = false;
        String custId = null;
        while (!login) {
            System.out.print("\n\t==Login==\t\n");
            String id = i.getString("Your Id", custId);
            String password = i.getString("Password", null);

            System.out.println(id + "\n" + password);
            login = rm.login(id, password);
            if (login) {
                custId = id;
            } else {
                System.out.println("\nWrong ID or Password, please try again\n");
            }
        }

        System.out.println("\nHello " + rm.showCustName(custId) + "!\n");
        System.out.println(rm.ListUnclaimedRedemption());
        try {
            List<Long> ids = i.makeClaim();
            for (Object o : ids) {
                Long id = (Long) o;
                System.out.println(rm.makeClaim(id));
            }
        } catch (RedemptionException ex) {
            System.out.println("\nERROR: Failed to make claim");
            System.out.println(ex.getMessage() + "\n");
        }
    }
}
