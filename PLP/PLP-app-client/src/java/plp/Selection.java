package plp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Selection {

    Logic logic;

    public Selection() {
        logic = new Logic();
    }

    public void plp(String[] args) {

        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        try {
            String choice = "";
            while (!choice.equals("0")) {
                logic = new Logic();
                displayMenu();
                choice = br.readLine();
                dispatch(choice);
            }
            return;
        } catch (Exception ex) {
            System.err.println("\nERROR: Caught an " +
                    "unexpected exception");
            ex.printStackTrace();
        }
    }

    public void displayMenu() {
        System.out.println("\n\n\t\tWELCOME TO PLP\n");
        System.out.println("Welcome to Point-based Loyalty Program");
        System.out.println("1. Add a customer");
        System.out.println("2. Delete a customer");
        System.out.println("3. Add a merchant");
        System.out.println("4. Delete a merchant");
        System.out.println("5. Add a reward type");
        System.out.println("6. Delete a reward type");
       // System.out.println("7. Add a redemption");
        System.out.println("7. Delete a redemption");
        System.out.println("8. Make a claim");
        System.out.println("0. Exit");
        System.out.print("\nPlease enter your choice: ");

    }

    public void dispatch(String choice) {
        if (choice.equals("1")) {
            logic.addCustomer();
        } else if (choice.equals("2")) {
            logic.deleteCustomer();
        } else if (choice.equals("3")) {
            logic.addMerchant();
        } else if (choice.equals("4")) {
            logic.deleteMerchant();
        } else if (choice.equals("5")) {
            logic.addRewardType();
        } else if (choice.equals("6")) {
            logic.deleteRewardType();
        } // else if (choice.equals("7")) {
        //   logic.addRedemption();
        //}
        else if (choice.equals("7")) {
            logic.deleteRedemption();
        } else if (choice.equals("8")) {
            logic.claim();
        } else if (choice.equals("0")) {
            return;
        } else {
            System.out.println("\nERROR: Invalid choice.");
        }
    }
}



