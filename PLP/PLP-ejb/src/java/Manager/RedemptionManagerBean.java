package Manager;

import appHelper.MerchantState;
import ejb.*;
import appHelper.RedemptionState;
import exception.ExistException;
import exception.RedemptionException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.jms.*;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.MessageProducer;
import javax.jms.MapMessage;
import javax.annotation.Resource;
import javax.ejb.EJB;

@Stateful
public class RedemptionManagerBean implements Manager.RedemptionManagerRemote {

    @PersistenceContext
    EntityManager em;
    private CustomerEntity cust;
    private static RedemptionEntity redemption;
    private static RedemptionLineEntity rl;
    private Collection<RedemptionLineEntity> redLines;
    private RewardTypeEntity reward;
    private ClaimEntity claim;
    @Resource(mappedName = "jms/QueueConnectionFactory")
    private ConnectionFactory queueConnectionFactory;
    @Resource(mappedName = "jms/Queue")
    private Queue queue;
    @EJB
    private MerchantManagerRemote mm;

    public RedemptionManagerBean() {
    }

    public boolean login(String id, String password) {
        if (em.find(CustomerEntity.class, id) == null) {
            return false;
        }
        cust = em.find(CustomerEntity.class, id);

        if (!cust.getCustPassword().equals(password)) {
            return false;
        }
        return true;
    }

    public String showCustName(String id) {
        return cust.getCustName();
    }
    /*
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String createRedemptionLine(Long id, int quantity) throws ExistException, RedemptionException, Exception {
    reward = em.find(RewardTypeEntity.class, id);

    if (reward == null) {
    em.clear();
    System.out.println("\nERROR");
    throw new ExistException("Reward Type does not exist");
    }


    int deductPoint = reward.getPointRequired() * quantity;
    deduceQuantity(quantity);
    try {
    deducePoint(deductPoint);
    } catch (RedemptionException ex) {
    ex.printStackTrace();
    System.out.println("\nERROR");
    throw new RedemptionException(RedemptionException.getINSUFFICIENT_POINT());
    }

    try {
    rl = new RedemptionLineEntity();
    rl.create(reward, quantity);

    redLines = redemption.getRedemptionLine();
    redLines.add(rl);
    redemption.setRedemptionLine(redLines);
    em.persist(redemption);
    } catch (Exception ex) {
    ex.printStackTrace();
    System.out.println("\nERROR");
    }

    return quantity + " of " + reward.getName() + " is added to redemption";
    }
     */

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createRedemption() {
        try {
            Calendar c = Calendar.getInstance();
            Date date = c.getTime();
            redemption = new RedemptionEntity();
            redLines = new ArrayList<RedemptionLineEntity>();
            redemption.setDate(date);
            redemption.setCustomer(cust);
            em.persist(redemption);
            em.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long createRedemption(String id, List<Long> ids, List<Integer> quantity, int total, List<String> thisMerId) throws ExistException, RedemptionException, Exception {
        cust = em.find(CustomerEntity.class, id);
        createRedemption();

        redLines = new ArrayList<RedemptionLineEntity>();
        for (int i = 0; i < ids.size(); i++) {

            Long rewardId = ids.get(i);

            reward = em.find(RewardTypeEntity.class, rewardId);
            if (reward == null) {
                em.clear();
                System.out.println("\nERROR");
                throw new ExistException("Reward Type does not exist");
            }
            //Get quantity
            Integer thisQuant = quantity.get(i);
            try {
                rl = new RedemptionLineEntity();
                rl.create(reward, thisQuant);
                em.persist(rl);
                em.flush();

                deduceQuantity(thisQuant);
                redLines = redemption.getRedemptionLine();
                redLines.add(rl);
                redemption.setRedemptionLine(redLines);
                MerchantState thisMer = mm.getThisMerchant(thisMerId.get(i));

                sendMessage(redemption.getRedId().toString() + rl.getId().toString(),
                        cust.getCustName(), cust.getContact().getAddress(),
                        reward.getId(), reward.getName(), thisQuant, thisMer.getName(),
                        thisMer.getAddress());
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("\nERROR");
            }
        }
        try {
            redemption.create(cust, redLines);
            em.merge(redemption);
            em.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            deducePoint(total);
        } catch (RedemptionException ex) {
            ex.printStackTrace();
            System.out.println("\nERROR");
            throw new RedemptionException(RedemptionException.getINSUFFICIENT_POINT());
        }

        return redemption.getRedId();

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String deleteRedemption(Long id) throws RedemptionException {
        redemption = em.find(RedemptionEntity.class, id);
        if (redemption == null) {
            em.clear();
            throw new RedemptionException(RedemptionException.getREDEMPTION_NOT_FOUND());
        }

        // String custId = redemption.getCustomer().getId();
        // cust = em.find(CustomerEntity.class, custId);

        int pointsReturn = 0;
        redLines = redemption.getRedemptionLine();
        Boolean claimed = false;
        ClaimEntity checkClaim = redemption.getClaim();
        if (checkClaim == null) {
            claimed = true;
        }

        for (Object o : redLines) {
            rl = (RedemptionLineEntity) o;
            Long rewardId = rl.getReward().getId();
            reward = em.find(RewardTypeEntity.class, rewardId);
            int pointRequire = reward.getPointRequired();
            int quantity = rl.getQuanityOfRewarType();
            pointsReturn = pointRequire * quantity;
            if (claimed) {
                returnQuantity(quantity);
                returnPoint(pointsReturn);
            }
            Long rlId = rl.getId();
            rl = em.find(RedemptionLineEntity.class, rlId);
            em.remove(rl);

        }
        em.remove(redemption);
        String response = "Redemption " + id + " is removed from database.";
        System.out.println(response);
        return response;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String makeClaim(Long id) throws RedemptionException {
        claim = new ClaimEntity();
        em.persist(claim);
        em.flush();
        redemption = em.find(RedemptionEntity.class, id);
        if (redemption == null) {
            em.clear();
            throw new RedemptionException(RedemptionException.getREDEMPTION_NOT_FOUND());
        }
        redemption.setClaim(claim);
        em.merge(redemption);
        em.flush();
        String response = "Redemption " + id + " is claimed";
        System.out.println(response);
        return response;
    }

    public String ListRedemption() {
        String redemptions = "";
        Query q = em.createQuery("select m from RedemptionEntity m");
        for (Object o : q.getResultList()) {
            redemption = (RedemptionEntity) o;
            redemptions =
                    redemptions +
                    "==================================" +
                    "\n" + "Redemption ID: " + redemption.getRedId() +
                    "\n";
            for (Object o2 : redemption.getRedemptionLine()) {
                rl = (RedemptionLineEntity) o2;
                redemptions =
                        redemptions + "Reward: " +
                        formatString(rl.getReward().getName(), 15) + "Quantity: " +
                        rl.getQuanityOfRewarType() + "\n";

            }

        }
        return redemptions;
    }

    public String ListUnclaimedRedemption() {

        String redemptions = "";
        Query q = em.createQuery("select m from RedemptionEntity m");
        for (Object o : q.getResultList()) {
            redemption = (RedemptionEntity) o;
            if (redemption.getClaim() == null) {
                redemptions =
                        redemptions +
                        "==================================" +
                        "\n" + "Redemption ID: " + redemption.getRedId() +
                        "\n";
                for (Object o2 : redemption.getRedemptionLine()) {
                    rl = (RedemptionLineEntity) o2;
                    redemptions =
                            redemptions + "Reward: " +
                            formatString(rl.getReward().getName(), 15) + "Quantity: " +
                            rl.getQuanityOfRewarType() + "\n";
                }
            }

        }
        return redemptions;
    }

    public void persist() {
        redemption.setRedemptionLine(redLines);
        em.merge(redemption);
        em.flush();
    }

    public List<RedemptionState> getRedemption() {
        Query q = em.createQuery("SELECT c FROM RedemptionEntity c");
        List stateList = new ArrayList();
        for (Object o : q.getResultList()) {
            RedemptionEntity r = (RedemptionEntity) o;
            RedemptionState rState = new RedemptionState(
                    r.getRedId(), r.getRedemptionLine(), r.getDate());
            stateList.add(rState);
        }

        return stateList;
    }

    public void sendMessage(String RedemptionInfoId, String CustomerName, String CustomerAddress, Long RewardID, String RewardName, int quantity, String MerchantName, String MerchantAddress) throws JMSException {
        Connection queueConnection = null;
        Session session = null;
        MapMessage message = null;
        MessageProducer producer = null;

        System.out.println(queueConnectionFactory);

        queueConnection = queueConnectionFactory.createConnection();
        session = queueConnection.createSession(false, 0);
        queueConnection.start();

        producer = session.createProducer(queue);
        message = session.createMapMessage();

        message.setString("RedemptionInfoId", RedemptionInfoId);
        message.setString("CustomerName", CustomerName);
        message.setString("CustomerAddress", CustomerAddress);
        message.setLong("RewardID", RewardID);
        message.setString("RewardName", RewardName);
        message.setInt("quantity", quantity);
        message.setString("MerchantName", MerchantName);
        message.setString("MerchantAddress", MerchantAddress);
        producer.send(message);
    }

    @Remove
    public void remove() {
        System.out.println("MerchantManagerBean:Remove()");
    }

    private void deduceQuantity(int quantity) {
        reward.setQuantityAvailable(reward.getQuantityAvailable() - quantity);
        em.merge(reward);
        em.flush();
    }

    private void returnQuantity(int quantity) {
        reward.setQuantityAvailable(reward.getQuantityAvailable() + quantity);
        em.merge(reward);
        em.flush();
    }

    private void deducePoint(int deductPoint) throws RedemptionException {
        cust.setCustPoints(cust.getCustPoints() - deductPoint);
        em.merge(cust);
        em.flush();
    }

    private void returnPoint(int returnPoint) {
        cust.setCustPoints(cust.getCustPoints() + returnPoint);
        em.merge(cust);
        em.flush();
    }

    private String formatString(String s, int len) {
        String out = s;
        for (int i = 0; i <
                (len - s.length()); i++) {
            out = out + " ";
        }
        return out;
    }
}
