package Manager;

import ejb.*;
import appHelper.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ejb.Remove;
import javax.persistence.Query;

@Stateful
public class RewardTypeMerchantManagerBean implements
        Manager.RewardTypeMerchantManagerRemote {

    @PersistenceContext
    EntityManager em;
    private RewardTypeEntity rewardType = new RewardTypeEntity();
    @EJB
    private MerchantManagerRemote mr;

    public RewardTypeMerchantManagerBean() {
    }

    public boolean createRewardType(String name, int point, int quantity) {
        if (checkExist(point)) {
            return false;
        } else {
            rewardType.create(name, point, quantity);
            return true;
        }
    }

    public boolean linkMerchants(HashSet<String> MerIds) {

        Set<MerchantEntity> merchant = new HashSet<MerchantEntity>();
        if (MerIds == null) {
            System.out.println("MerId is null");
            return false;
        }
        for (Object o : MerIds) {
            String MerId = (String) o;

            if (em.find(MerchantEntity.class, MerId) == null) {
                return false;
            } else {
                em.merge(em.find(MerchantEntity.class, MerId));
                merchant.add(em.find(MerchantEntity.class, MerId));
                System.out.println(MerId + "added");
            }
        }
        rewardType.setMerchant(merchant);

        return true;
    }

    public List<String> getRewardMerchantID(String id) {
        List<String> result = new ArrayList<String>();
        Long ID = Long.parseLong(id);
        RewardTypeEntity r = em.find(RewardTypeEntity.class, ID);
        Set<MerchantEntity> mer = r.getMerchant();

        for (Object o : mer) {
            MerchantEntity merchant = (MerchantEntity) o;
            result.add(merchant.getId());
        }

        return result;
    }

    public List<String> getRewardMerchantName(String id) {
        List<String> result = new ArrayList<String>();
        Long ID = Long.parseLong(id);
        RewardTypeEntity r = em.find(RewardTypeEntity.class, ID);
        Set<MerchantEntity> mer = r.getMerchant();

        for (Object o : mer) {
            MerchantEntity merchant = (MerchantEntity) o;
            result.add(merchant.getName());
        }

        return result;
    }

    public String deleteRewardType(Long id) {
        try {
            em.remove(em.getReference(RewardTypeEntity.class, id));
            String response = "RewardType " + id + " is removed from database.";
            System.out.println(response);
            return response;
        } catch (Exception ex) {
            String response = "RewardType " + id + " doesn't exist.";
            System.out.println(response);
            return response;
        }
    }

    public String ListRewardType() {

        String rewardType = "";
        Query q = em.createQuery("select m from RewardTypeEntity m");
        for (Object o : q.getResultList()) {
            RewardTypeEntity m = (RewardTypeEntity) o;
            rewardType = rewardType +
                    "Reward Type id:      " + formatString(m.getId().toString(), 10) +
                    "Reward Type name:    " + formatString(m.getName(), 20) +
                    "Point required: " + m.getPointRequired() + "\n";
        }
        return rewardType;
    }

    public void persist() {
        em.persist(rewardType);
        em.flush();
    }

    private String formatString(String s, int len) {
        String out = s;

        for (int i = 0; i < (len - s.length()); i++) {
            out = out + " ";
        }
        return out;
    }

    public List<RewardTypeState> getRewardType() {
        Query q = em.createQuery("select m from RewardTypeEntity m");
        List stateList = new ArrayList();
        for (Object o : q.getResultList()) {
            RewardTypeEntity r = (RewardTypeEntity) o;
            RewardTypeState rState = new RewardTypeState(
                    r.getId(), r.getName(), r.getPointRequired(), r.getQuantityAvailable());
            stateList.add(rState);
        }
        return stateList;
    }

    public String getRewardName(Long id) {
        String rewardName = null;
        for (Object o : getRewardType()) {
            RewardTypeState r = (RewardTypeState) o;
            if (r.getId().equals(id)) {
                rewardName = r.getName();
            }
        }
        return rewardName;
    }

    @Remove
    public void remove() {
        System.out.println("MerchantManagerBean:Remove()");
    }

    public boolean checkExist(int point) {
        for (int i = 0; i < this.getRewardType().size(); i++) {
            if (this.getRewardType().get(i).getPointRequired() == point) {
                return true;
            }
        }
        return false;
    }
}
