package Manager;

import appHelper.RewardTypeState;
import java.util.HashSet;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface RewardTypeMerchantManagerRemote {

    public boolean createRewardType(String name, int point, int quantity);

    public boolean linkMerchants(HashSet<String> MerIds);

    public String deleteRewardType(Long id);

    public String ListRewardType();

    public void persist();

    public List<String> getRewardMerchantID(String id);

    public List<String> getRewardMerchantName(String id);

    public List<RewardTypeState> getRewardType();

    public String getRewardName(Long id);

    public void remove();

    public boolean checkExist(int point);
}
