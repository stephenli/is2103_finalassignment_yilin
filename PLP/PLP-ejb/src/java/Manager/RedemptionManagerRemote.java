/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Manager;

import appHelper.RedemptionState;
import exception.ExistException;
import exception.RedemptionException;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author liyilin
 */
@Remote
public interface RedemptionManagerRemote {

    public boolean login(String id, String password);

    public String showCustName(String id);

    // public String createRedemptionLine(Long id, int quantity) throws ExistException, RedemptionException, Exception;
    public String makeClaim(Long id) throws RedemptionException;

    public String ListUnclaimedRedemption();

    public Long createRedemption(String username, List<Long> ids, List<Integer> quantity, int total, List<String> thisMerId) throws ExistException, RedemptionException, Exception;

    public String deleteRedemption(Long id) throws RedemptionException;

    public String ListRedemption();

    public void createRedemption();

    public void persist();

    public List<RedemptionState> getRedemption();

    public void remove();
}
