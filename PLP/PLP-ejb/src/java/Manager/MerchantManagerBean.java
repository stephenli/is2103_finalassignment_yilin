package Manager;

import ejb.*;
import appHelper.MerchantState;
import exception.ExistException;
import exception.MerchantException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ejb.Remove;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

@Stateful
public class MerchantManagerBean implements
        MerchantManagerRemote {

    @PersistenceContext
    EntityManager em;
    private MerchantEntity merchant;

    public MerchantManagerBean() {
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createMerchant(String id, String name, String address) throws ExistException {
        merchant = em.find(MerchantEntity.class, id);
        if (merchant != null) {
            throw new ExistException("MERCHANT ALREADY EXIST.");
        }

        merchant = new MerchantEntity();
        merchant.create(id, name, address);
        em.persist(merchant);

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteMerchant(String id) throws ExistException, MerchantException {
        merchant = em.find(MerchantEntity.class, id);
        if (merchant == null) {
            throw new ExistException("MERCHANT DOES NOT EXIST");
        }
        Query q = em.createQuery("select r from RewardTypeEntity r");
        for (Object o : q.getResultList()) {
            RewardTypeEntity r = (RewardTypeEntity) o;
            for (Object o2 : r.getMerchant()) {
                MerchantEntity m = (MerchantEntity) o2;
                if (m.getId().equals(id)) {
                    throw new MerchantException(MerchantException.getHAS_REWARD());
                }
            }
        }
        em.remove(merchant);
    }

    public MerchantState getThisMerchant(String id){
        MerchantState thisMer=null;
        for(Object o:getMerchant()){
            MerchantState mer=(MerchantState)o;
            if(mer.getId().equals(id))
                thisMer=mer;
        }
        return thisMer;
    }
    public List<MerchantState> getMerchant() {
        Query q = em.createQuery("select m from MerchantEntity m");
        List stateList = new ArrayList();
        for (Object o : q.getResultList()) {
            MerchantEntity m = (MerchantEntity) o;
            MerchantState mState = new MerchantState(
                    m.getId(), m.getName(), m.getAddress());
            stateList.add(mState);
        }
        return stateList;
    }

    public String ListMerchants() {
        String merchants = "";
        for (Object o : getMerchant()) {
            MerchantState m = (MerchantState) o;
            merchants = merchants +
                    "Merchant id:      " + formatString(m.getId(), 15) +
                    "Merchant name:    " + m.getName() + "\n";
        }
        return merchants;
    }

    public boolean checkExist(String id) {
        if (em.find(MerchantEntity.class, id) == null) {
            return false;
        } else {
            return true;
        }
    }

    private String formatString(String s, int len) {
        String out = s;

        for (int i = 0; i < (len - s.length()); i++) {
            out = out + " ";
        }
        return out;
    }

    @Remove
    public void remove() {
        System.out.println("MerchantManagerBean:Remove()");
    }
}
