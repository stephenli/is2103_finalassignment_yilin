package Manager;

import ejb.*;
import javax.ejb.Remote;
import appHelper.CustomerContactState;
import exception.*;
import java.util.List;

@Remote
public interface CustomerContactManagerRemote {

    public void createCustomer(String passportNo, String name, String password, String address) throws ExistException;

    public ContactEntity createContact(String address);

    public void deleteCustomer(String id) throws ExistException, CustomerException;

    public void persist();

    public List<CustomerContactState> getCustomers();

    public void remove();

    public boolean login(String id, String password);

    public CustomerContactState getCustomer(String id);

    public String ListCustomers();
}
