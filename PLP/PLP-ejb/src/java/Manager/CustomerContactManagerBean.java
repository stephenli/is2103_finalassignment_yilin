package Manager;

import ejb.*;
import appHelper.CustomerContactState;
import exception.CustomerException;
import exception.ExistException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateful
public class CustomerContactManagerBean implements
        CustomerContactManagerRemote {

    @PersistenceContext
    EntityManager em;
    private CustomerEntity customer;
    private ContactEntity contact;

    public CustomerContactManagerBean() {
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createCustomer(String id, String name, String password,
            String address) throws ExistException {

        customer = em.find(CustomerEntity.class, id);
        if (customer != null) {
            System.out.println("\nCUSTOMER ALREADY EXIST.\n");
            throw new ExistException("CUSTOMER ALREADY EXIST.");
        }
        try {
            customer = new CustomerEntity();
            customer.create(id, name, password);
            contact = createContact(address);
            customer.setContact(contact);
        } catch (Exception ex) {
            System.out.println("\nERROR: \n");
            ex.printStackTrace();
        }
        em.persist(customer);
    }

    public ContactEntity createContact(String address) {
        contact = new ContactEntity();
        contact.create(address);
        return contact;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteCustomer(String id) throws ExistException, CustomerException {
        customer = em.find(CustomerEntity.class, id);
        if (customer == null) {
            throw new ExistException("CUSTOMER DOES NOT EXIST");
        }
        if (customer.getRedemption().size() > 0) {
            throw new CustomerException(CustomerException.getHAS_REDEMPTION());
        }
        em.remove(customer);

    }

    public String ListCustomers() {

        String customers = "";
        Query q = em.createQuery("select m from CustomerEntity m");
        for (Object o : q.getResultList()) {
            CustomerEntity m = (CustomerEntity) o;
            customers = customers +
                    "Customer id:      " + formatString(m.getId(), 10) +
                    "Customer name:    " + m.getCustName() + "\n";
        }
        return customers;
    }

    public void persist() {
        customer.setContact(contact);
        em.persist(customer);

    }

    public List<CustomerContactState> getCustomers() {
        Query q = em.createQuery("SELECT c FROM CustomerEntity c");
        List stateList = new ArrayList();
        for (Object o : q.getResultList()) {
            CustomerEntity c = (CustomerEntity) o;
            CustomerContactState ccState = new CustomerContactState(
                    c.getId(), c.getCustName(), c.getCustPoints(),
                    c.getContact().getContactId(),
                    c.getContact().getAddress());
            stateList.add(ccState);
        }
        return stateList;
    }

    @Remove
    public void remove() {
        System.out.println("CustomerContactManagerBean:Remove()");
    }

    private String formatString(String s, int len) {
        String out = s;

        for (int i = 0; i < (len - s.length()); i++) {
            out = out + " ";
        }
        return out;
    }

    public boolean login(String id, String password) {
        if (em.find(CustomerEntity.class, id) == null) {
            return false;
        }
        customer = em.find(CustomerEntity.class, id);

        if (!customer.getCustPassword().equals(password)) {
            return false;
        }
        return true;
    }

    public CustomerContactState getCustomer(String id) {
        for (Object o : getCustomers()) {
            CustomerContactState cust = (CustomerContactState) o;
            if (cust.getCustomerId().equals(id)) {
                return cust;
            }

        }
        return null;
    }
}


