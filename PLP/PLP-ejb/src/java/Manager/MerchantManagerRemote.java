package Manager;

import javax.ejb.Remote;
import appHelper.MerchantState;
import exception.ExistException;
import exception.MerchantException;
import java.util.List;

@Remote
public interface MerchantManagerRemote {

    public void createMerchant(String id, String name, String address) throws ExistException;

    public void deleteMerchant(String id) throws ExistException, MerchantException;

    public List<MerchantState> getMerchant();

    public void remove();

    public MerchantState getThisMerchant(String id);

    public String ListMerchants();
}
