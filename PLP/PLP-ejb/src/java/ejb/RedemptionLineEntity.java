/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
/**
 *
 * @author liyilin
 */
@Entity
public class RedemptionLineEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long RLid;
    private int QuanityOfRewarType;

    @ManyToOne(cascade={CascadeType.PERSIST})
    private RewardTypeEntity reward=new RewardTypeEntity();

    public RedemptionLineEntity() {
    }

    public void create(RewardTypeEntity reward,int QuanityOfRewarType) {
        this.setQuanityOfRewarType(QuanityOfRewarType);
        this.setReward(reward);
    }

    public Long getId() {
        return RLid;
    }

    public void setId(Long id) {
        this.RLid = id;
    }

    public int getQuanityOfRewarType() {
        return QuanityOfRewarType;
    }

    public void setQuanityOfRewarType(int QuanityOfRewarType) {
        this.QuanityOfRewarType = QuanityOfRewarType;
    }

    public RewardTypeEntity getReward() {
        return reward;
    }

    public void setReward(RewardTypeEntity reward) {
        this.reward = reward;
    }


}
