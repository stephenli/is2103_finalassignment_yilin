/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;


@Entity
public class ClaimEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ClaimId;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date ClaimDate;

    @OneToOne(mappedBy="claim")
    private RedemptionEntity redemption;

    public ClaimEntity() {
        Calendar c=Calendar.getInstance();
        setClaimDate(c.getTime());
    }

    public Long getId() {
        return ClaimId;
    }

    public void setId(Long id) {
        this.ClaimId = id;
    }

    public RedemptionEntity getRedemption() {
        return redemption;
    }

    public void setRedemption(RedemptionEntity redemption) {
        this.redemption = redemption;
    }

    public Date getClaimDate() {
        return ClaimDate;
    }

    public void setClaimDate(Date ClaimDate) {
        this.ClaimDate = ClaimDate;
    }

}
