package ejb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

@Entity
public class RedemptionEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long RedId;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    @OneToOne(cascade = {CascadeType.PERSIST})
    private ClaimEntity claim;
    @OneToMany(cascade = {CascadeType.PERSIST})
    private Collection<RedemptionLineEntity> redemptionLine = new ArrayList<RedemptionLineEntity>();
    @ManyToOne
    private CustomerEntity customer = new CustomerEntity();

    public RedemptionEntity() {
    }

    public void create(CustomerEntity customer, Collection<RedemptionLineEntity> redemptionLine) {
        this.setCustomer(customer);
        this.setRedemptionLine(redemptionLine);
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        this.setDate(date);
    }

    public ClaimEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimEntity claim) {
        this.claim = claim;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public Collection<RedemptionLineEntity> getRedemptionLine() {
        return redemptionLine;
    }

    public void setRedemptionLine(Collection<RedemptionLineEntity> redemptionLine) {
        this.redemptionLine = redemptionLine;
    }

    public void create(Date date) {
        this.date = date;
    }

    public Long getRedId() {
        return RedId;
    }

    public void setRedId(Long RedId) {
        this.RedId = RedId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
