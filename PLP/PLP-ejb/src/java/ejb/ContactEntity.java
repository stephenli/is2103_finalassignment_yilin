/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ContactEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ContactId;
    private String address;

    public ContactEntity() {

    }
    public void create(String address){
    this.address=address;
    }

    public Long getContactId() {
        return ContactId;
    }

    public void setContactId(Long ContactId) {
        this.ContactId = ContactId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



}
