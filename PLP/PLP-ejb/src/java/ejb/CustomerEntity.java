
package ejb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class CustomerEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String CustomerId;
    private String CustName;
    private String CustPassword;
    private int CustPoints;

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private ContactEntity contact;

    @OneToMany(cascade={CascadeType.PERSIST},mappedBy="customer")
    private Collection<RedemptionEntity> redemption
            =new ArrayList<RedemptionEntity>();


    public CustomerEntity() {
        this.CustPoints = 500;
    }

    public ContactEntity getContact() {
        return contact;
    }

    public void setContact(ContactEntity contact) {
        this.contact = contact;
    }

    public void create(String passportNo, String CustName, String CustPassword) {
        this.CustomerId = passportNo;
        this.CustName = CustName;
        this.CustPassword = CustPassword;
    }
    public Collection<RedemptionEntity> getRedemption() {
        return redemption;
    }

    public void setRedemption(Collection<RedemptionEntity> redemption) {
        this.redemption = redemption;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String CustName) {
        this.CustName = CustName;
    }

    public String getCustPassword() {
        return CustPassword;
    }

    public void setCustPassword(String CustPassword) {
        this.CustPassword = CustPassword;
    }

    public int getCustPoints() {
        return CustPoints;
    }

    public void setCustPoints(int CustPoints) {
        this.CustPoints = CustPoints;
    }

    public String getId() {
        return CustomerId;
    }

    public void setId(String CustomerId) {
        this.CustomerId = CustomerId;
    }


}
