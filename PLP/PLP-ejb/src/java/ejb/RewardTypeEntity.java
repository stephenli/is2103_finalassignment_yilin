/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;


@Entity
public class RewardTypeEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private int pointRequired;
    private int quantityAvailable;

    @ManyToMany
    private Set<MerchantEntity> merchant
            =new HashSet<MerchantEntity>();

    public RewardTypeEntity() {
    }

    public void create(String name, int pointRequired,int quantityAvailable){
    this.setName(name);
    this.setPointRequired(pointRequired);
    this.setQuantityAvailable(quantityAvailable);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<MerchantEntity> getMerchant() {
        return merchant;
    }

    public void setMerchant(Set<MerchantEntity> merchant) {
        this.merchant = merchant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPointRequired() {
        return pointRequired;
    }

    public void setPointRequired(int pointRequired) {
        this.pointRequired = pointRequired;
    }

    public int getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(int quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }


}
