/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class InitializationException extends Exception {

    public InitializationException() {
    }

    public InitializationException(String message) {
        super(message);
    }


}
