package exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author liyilin
 */
@ApplicationException(rollback = true)
public class RedemptionException extends Exception{

    private int exception = 0;
    private static final int INSUFFICIENT_POINT = 1;
    private static final int REDEMPTION_NOT_FOUND = 2;

    public RedemptionException() {
    }

    public RedemptionException(int ex) {
        setException(ex);
    }

    public static int getINSUFFICIENT_POINT() {
        return INSUFFICIENT_POINT;
    }

    public static int getREDEMPTION_NOT_FOUND() {
        return REDEMPTION_NOT_FOUND;
    }

    public int getException() {
        return exception;
    }

    public void setException(int exception) {
        this.exception = exception;
    }
}
