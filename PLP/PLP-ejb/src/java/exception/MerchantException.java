/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exception;
import javax.ejb.ApplicationException;

/**
 *
 * @author liyilin
 */
@ApplicationException(rollback = true)

public class MerchantException extends Exception{
private int exception = 0;
    private static final int HAS_REDEMPTION = 1;

    public MerchantException() {
    }

    public MerchantException(int exception) {
        setException(exception);
    }

    public int getException() {
        return exception;
    }

    public void setException(int exception) {
        this.exception = exception;
    }
    public static int getHAS_REWARD(){
    return HAS_REDEMPTION;
}
}
