/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package appHelper;

import ejb.ClaimEntity;
import ejb.CustomerEntity;
import ejb.MerchantEntity;
import ejb.RedemptionLineEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 * @author liyilin
 */
public class ReportState {

    private Long reportId;
    private String custName;
    private List<Long> rewardId;
    private List<Integer> quantity;
    private List<String> thisMerchant;
    private boolean claimed=false;
    private Date claimDate=null;

    public ReportState(Long reportId, String custName, List<Long> rewardId, List<Integer> quantity, List<String> thisMerchant) {
        this.reportId = reportId;
        this.custName = custName;
        this.rewardId = rewardId;
        this.quantity = quantity;
        this.thisMerchant = thisMerchant;
    }

    public Date getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(Date claimDate) {
        this.claimDate = claimDate;
    }

    public boolean isClaimed() {
        return claimed;
    }

    public void setClaimed(boolean claimed) {
        this.claimed = claimed;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public List<Integer> getQuantity() {
        return quantity;
    }

    public void setQuantity(List<Integer> quantity) {
        this.quantity = quantity;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public List<Long> getRewardId() {
        return rewardId;
    }

    public void setRewardId(List<Long> rewardId) {
        this.rewardId = rewardId;
    }

    public List<String> getThisMerchant() {
        return thisMerchant;
    }

    public void setThisMerchant(List<String> thisMerchant) {
        this.thisMerchant = thisMerchant;
    }
    

}