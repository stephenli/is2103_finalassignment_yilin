/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package appHelper;

import java.io.Serializable;

/**
 *
 * @author liyilin
 */
public class CustomerContactState implements Serializable{

    private Long ContactId;
    private String address;
    private String CustomerId;
    private String name;
    private String password;
    private int points;

    public CustomerContactState() {
    }
    

    public CustomerContactState(String CustomerId, String name, int points,
            Long ContactId, String address) {
        this.setCustomerId(CustomerId);
        this.setContactId(ContactId);
        this.setAddress(address);
        this.setName(name);
        this.setPassword(password);
        this.setPoints(points);

    }

    public Long getContactId() {
        return ContactId;
    }

    public void setContactId(Long ContactId) {
        this.ContactId = ContactId;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String CustomerId) {
        this.CustomerId = CustomerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
