/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package appHelper;

import ejb.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author liyilin
 */
public class RedemptionState {

    private Long RedId;
    private Date date;
    private ClaimEntity claim;
    private Collection<RedemptionLineEntity> redemptionLine = new ArrayList<RedemptionLineEntity>();
    private CustomerEntity customer = new CustomerEntity();

    public RedemptionState(Long id, Collection<RedemptionLineEntity> rl,Date date) {
        this.setRedId(id);
        this.setDate(date);
        this.setRedemptionLine(rl);
    }

    public Long getRedId() {
        return RedId;
    }

    public void setRedId(Long RedId) {
        this.RedId = RedId;
    }

    public ClaimEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimEntity claim) {
        this.claim = claim;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Collection<RedemptionLineEntity> getRedemptionLine() {
        return redemptionLine;
    }

    public void setRedemptionLine(Collection<RedemptionLineEntity> redemptionLine) {
        this.redemptionLine = redemptionLine;
    }


}
