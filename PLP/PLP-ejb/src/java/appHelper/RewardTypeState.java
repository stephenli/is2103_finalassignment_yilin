

package appHelper;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


public class RewardTypeState implements Serializable{

    private Long id;
    private String name;
    private int pointRequired;
    private int quantityAvailable;
    private Set<MerchantState> merchant=new HashSet<MerchantState>();

    public RewardTypeState( Long id,String name, int pointRequired, int quantityAvailable) {
        this.setId(id);
        this.setName(name);
        this.setPointRequired(pointRequired);
        this.setQuantityAvailable(quantityAvailable);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<MerchantState> getMerchant() {
        return merchant;
    }

    public void setMerchant(Set<MerchantState> merchant) {
        this.merchant = merchant;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPointRequired() {
        return pointRequired;
    }

    public void setPointRequired(int pointRequired) {
        this.pointRequired = pointRequired;
    }

    public int getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(int quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }


}
