package servlet;

import Manager.CustomerContactManagerRemote;
import Manager.MerchantManagerRemote;
import Manager.RedemptionManagerRemote;
import Manager.RewardTypeMerchantManagerRemote;
import appHelper.CustomerContactState;
import appHelper.ReportState;
import appHelper.RewardTypeState;
import exception.ExistException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PLPServlet extends HttpServlet {

    @EJB
    private CustomerContactManagerRemote ccm;
    @EJB
    private RedemptionManagerRemote rm;
    @EJB
    private RewardTypeMerchantManagerRemote rmm;
    @EJB
    private MerchantManagerRemote mm;
    ReportState report;
    private CustomerContactState cust = null;
    private String userName;
    private boolean valid;

    public void init() {

        System.out.println("PLPServlet: init()");
    }

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("SecondBankServlet: processRequest()");
        try {
            RequestDispatcher dispatcher;
            ServletContext servletContext = getServletContext();
            String page = request.getPathInfo();
            System.out.println(page);
            page = page.substring(1);
            System.out.println(page);
            if ("Login".equals(page)) {
            } else if ("custInfo".equals(page)) {
                Login(request);
                if (!valid) {
                    page = "LoginFail";
                }
            } else if ("makeRedemption".equals(page)) {
                try {
                    custInfo(request);
                    selectRedemption(request);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if ("result".equals(page)) {
                try {
                    makeRedemption(request);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            } else if ("report".equals(page)) {
                try {

                    report(request);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if ("LoginFail".equals(page)) {
                //do nothing
            } else {
                page = "Error";
            }

            dispatcher = servletContext.getNamedDispatcher(page);
            if (dispatcher == null) {
                dispatcher = servletContext.getNamedDispatcher("Error");
            }
            System.out.println(request.getContextPath());
            System.out.println(request.toString());
            System.out.println(dispatcher.toString());
            System.out.println(page);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            log("Exception in PLPServlet.processRequest()");
        }
    }

    private void Login(HttpServletRequest request) {
        valid = false;

        System.out.println("ENTER LOGIN PHASE: ");

        String id = request.getParameter("id");

        System.out.println(id);
        String password = request.getParameter("password");
        System.out.println(password);

        if (ccm.login(id, password)) {
            System.out.println("Login in success");
            cust =
                    ccm.getCustomer(id);
            valid =
                    true;
            request.setAttribute("customer", cust);
        }

    }

    private void custInfo(HttpServletRequest request) {
        request.setAttribute("customer", cust);
        request.setAttribute("points", cust.getPoints());
        userName =cust.getName();
        request.setAttribute("name", userName);
    }

    private void selectRedemption(HttpServletRequest request) {
        List<RewardTypeState> rewards = rmm.getRewardType();
        request.setAttribute("rewards", rewards);

        for (Object o : rewards) {
            RewardTypeState rs = (RewardTypeState) o;
            Long id = rs.getId();

            List<String> result1 = rmm.getRewardMerchantID(id.toString());
            List<String> result2 = rmm.getRewardMerchantName(id.toString());

            request.setAttribute(id.toString() + "_merchantID", result1);
            request.setAttribute(id.toString() + "_merchantName", result2);
        }

    }

    private void makeRedemption(HttpServletRequest request) throws ExistException {
        int total = 0;
        int i = 0;
        request.setAttribute("name", cust.getName());
        List<RewardTypeState> rewards = rmm.getRewardType();
        List<Long> ids = new ArrayList<Long>();
        List<Integer> quantity = new ArrayList<Integer>();
        List<String> merchantId = new ArrayList<String>();
        List<String> merchantName = new ArrayList<String>();
        for (Object o : rewards) {
            RewardTypeState rs = (RewardTypeState) o;
            long rewardId = rs.getId();
            String amount = Long.toString(rewardId) + "_num";
            String merchant = Long.toString(rewardId) + "_merchant";

            if (request.getParameter(Long.toString(rewardId)) != null &&
                    request.getParameter(Long.toString(rewardId)).equals("on")) {
                ids.add(rewardId);
                quantity.add(Integer.parseInt(request.getParameter(amount)));
                merchantId.add(request.getParameter(merchant));
                total =total + Integer.parseInt((String) request.getParameter(amount))
                        * rs.getPointRequired();
            }

            System.out.print(i++);
        }
        request.setAttribute("total", total);
        String username = cust.getCustomerId();
        int customer_points = cust.getPoints();
        request.setAttribute("customer_points", customer_points);
        if (customer_points < total) {
            request.setAttribute("valid", "NO");
        } else {
            try {
                for (Object o : merchantId) {
                    String mId = (String) o;
                    merchantName.add(mm.getThisMerchant(mId).getName());
                }
                System.out.println("===========================");
                System.out.println("Username: "+username);
                System.out.println("Ids: "+ids.toString());
                System.out.println("Quantity: "+quantity.toString());
                System.out.println("Total: "+total);
                System.out.println("MerchantId: "+merchantId.toString());

                Long getId = rm.createRedemption(username, ids, quantity,total,merchantId);
                System.out.println("Redemption id: "+getId);
                request.setAttribute("ID", getId);
                report = new ReportState(getId, cust.getName(), ids, quantity, merchantName);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void report(HttpServletRequest request) {
        try {
            if (report.isClaimed()) {
                request.setAttribute("claimed", "Claimed");
            } else {
                request.setAttribute("claimed", "Not claimed");
            }
            request.setAttribute("redemotionId", report.getReportId());
            request.setAttribute("custName", report.getCustName());
            Integer size = Integer.valueOf(report.getRewardId().size());

            request.setAttribute("size", size.toString());
            System.out.println("===================");
            System.out.println("Size: "+size);
            for (int i = 0; i < size; i++) {
                String rewardName=rmm.getRewardName(report.getRewardId().get(i));
                request.setAttribute("reward_" + i, rewardName);
                request.setAttribute("quantity_" + i, report.getQuantity().get(i).toString());
                request.setAttribute("thisMerchant_" + i, report.getThisMerchant().get(i));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("PLPServlet: doGet()");
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("PLPServlet: doPost()");
        processRequest(request, response);
    }

    public void destroy() {
        System.out.println("PLPServlet: destroy()");
    }
}
