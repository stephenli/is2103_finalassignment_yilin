<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@page import="java.util.*" %>
<%@page import="appHelper.RewardTypeState;" %>
<%@page import="ejb.MerchantEntity;" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reward Selection</title>
    </head>
    <body>
         <h3>JSP <font color=olive>makeRedemption.jsp</font> at
            <font color=blue>
                <%= request.getContextPath()%>
            </font>
            <hr><br>
        </h3>
        <h2 align="left">
            Hello, <%= request.getAttribute("name")%>
        </h2>
        <h3 align="left"><b>You have </b><i style="color:red">
                <%= request.getAttribute("points")%>
            </i><b> point avaiblable</b></h3>
        <form name="form" action="result" method="post">
            <input type="hidden" name="username"  value="
                   <%=request.getParameter("username")%>" />
            <table align="left" border="2" style="font-weight: bolder">
                <tr><th></th>
                    <th>Name:</th>
                    <th>Points:</th>
                    <th>Availability</th>
                    <th>Merchants</th>
                    <th>number</th></tr>
                    <% List<RewardTypeState> rewards = new ArrayList<RewardTypeState>();
            rewards = (List<RewardTypeState>) request.getAttribute("rewards");
            for (Object o : rewards) {
                RewardTypeState rs = (RewardTypeState) o;
                    %>
                <tr>
                    <td><input type="checkbox" name="<%= rs.getId()%>"></td><td><%= rs.getName()%></td>
                    <td align="left">
                    <%= rs.getPointRequired()%></td><td align="center"><%= rs.getQuantityAvailable()%></td>
                    <td>
                        <select name="<%= rs.getId() + "_merchant"%>" style="width:150px;">
                            <%
                List<String> result1 = (List<String>) request.getAttribute(rs.getId() + "_merchantName");
                List<String> result2 = (List<String>) request.getAttribute(rs.getId() + "_merchantID");
                for (int i = 0; i < result1.size(); i++) {
                            %>
                            <option value="<%= result2.get(i)%>"><%= result1.get(i)%></option>
                            <%
                }
                            %>
                        </select>
                    </td>
                    <td><input type="text" name="<%= rs.getId() + "_num"%>" /></td>
                </tr>
                <%
            }
                %>
                <tr>
                    <td colspan="6" align="left"><input type="submit" value="submit" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>
