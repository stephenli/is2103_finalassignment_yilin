<%--
    file: custInfo.jsp
--%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList, appHelper.CustomerContactState" %>

<html>
    <head>
        <title> Customers Information </title>
    </head>
    <body>
         <h3>JSP <font color=olive>custInfo.jsp</font> at
            <font color=blue>
                <%= request.getContextPath()%>
            </font>
            <hr><br>
        </h3>
        <form action="makeRedemption" method="POST" >
            <h1> Customers Information </h1>
            <%
                CustomerContactState cs = (CustomerContactState) request.getAttribute("customer");
            %>
            <p>Id         = <%= cs.getCustomerId()%>
            <p>Name       = <%= cs.getName()%>
            <p>Address    = <%= cs.getAddress()%>
            <p>
            <p>
                <br>
                <font color=navy>
                    <h2>Select Choice</h2>
                    <tr><input type="submit" value="makeRedemption" name="makeRedemption"/></tr>
                </font>
        </form>
    </body>
</html>
