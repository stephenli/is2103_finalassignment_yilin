<%@page contentType="text/html" pageEncoding="UTF-8"%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>JSP <font color=olive>result.jsp</font> at
            <font color=blue>
                <%= request.getContextPath()%>
            </font>
            <hr><br>
        </h3>
        <form action="report" method="POST">
            <h2>Hello <%=request.getAttribute("name")%>!</h2>
            <p>
            <p>
            <h3>Total point deducted: <%= request.getAttribute("total")%></h3>
            <h3>Your point left:<%= ((Integer) request.getAttribute("customer_points") -
                        (Integer) request.getAttribute("total"))%></h3>
            <%
            if ("NO".equals(request.getAttribute("valid"))) {
            %>
            <h3 style="color:olive">Please select rewards lower than your total points</h3>
            <%
                ;
            }
            %> <tr><input type="submit" value="result"/></tr>
    </form>
</body>
</html>
