

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="appHelper.RewardTypeState;" %>
<%@page import="ejb.MerchantEntity;" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>report</title>
    </head>
    <body>
        <h3>JSP <font color=olive>makeRedemption.jsp</font> at
            <font color=blue>
                <%= request.getContextPath()%>
            </font>
            <hr><br>
        </h3>
        <h2 align="left">Redemption report</h2>
        <p></p>
        <p></p>
        <h3>=============================================================</h3>
        <h3>Redemption ID: <%=request.getAttribute("redemotionId")%></h3>
        <h3>Customer Name: <%=request.getAttribute("custName")%></h3>
        <h3>=============================================================</h3>
        <%
            for (int i = 0; i < Integer.parseInt((String) request.getAttribute("size")); i++) {
        %>
        <h3>Reward: <%=request.getAttribute("reward_" + i)%></h3>
        <h3>Quantity: <%=request.getAttribute("quantity_" + i)%></h3>
        <h3>Merchant: <%=request.getAttribute("thisMerchant_" + i)%></h3>
        <h3>===========</h3>
        <%;
            }%>
        <h3>
            Claim Status: <%=request.getAttribute("claimed")%>
        </h3>


    </body>
</html>

